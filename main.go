package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sns"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
)

func main() {
	log.Println("Iniciando lambda...")
	lambda.Start(Handler)
}

func Handler(ctx context.Context) (string, error) {
	response := PunchTheClock()

	if response["status"] == "OK" {
		Publish(ctx, response)
		return "Lambda executada com sucesso", nil
	} else {
		descricao := fmt.Sprintf("%v", response["descricao"])
		log.Fatalln(descricao)
		return "Erro ao efetuar batida de ponto: " + descricao, nil
	}

}

func PunchTheClock() map[string]any {

	log.Println("Getting environments...")

	endpoint := os.Getenv("URL")
	registration := os.Getenv("MATRICULA")
	password := os.Getenv("SENHA")
	flag := os.Getenv("SHOW_CREDENTIALS")
	latitude := os.Getenv("LATITUDE")
	longitude := os.Getenv("LONGITUDE")

	if flag == "on" {
		log.Println(endpoint, registration, password)
	}

	data := url.Values{}

	data.Add("matricula", registration)
	data.Add("senha", password)
	data.Add("latitude", latitude)
	data.Add("longitude", longitude)
	data.Add("grupo", "/pontoeletronico")
	data.Add("timezone", "America/Bahia")

	log.Println("Sending request")

	response, err := http.PostForm(endpoint, data)

	if err != nil {
		panic("configuration error, " + err.Error())
	}

	text, err := io.ReadAll(response.Body)
	if err != nil {
		panic("Error: " + err.Error())
	}

	log.Println(string(text))
	log.Printf("Http status: %d\n", response.StatusCode)
	var responseMap map[string]any

	err = json.Unmarshal(text, &responseMap)
	if err != nil {
		panic("Error on json unmarshal" + err.Error())
	}

	log.Println(responseMap)
	defer response.Body.Close()

	return responseMap
}

func Publish(ctx context.Context, response map[string]any) {
	topicArn := os.Getenv("TOPIC_ARN")

	data := fmt.Sprintf("%v", response["data"])
	hora := fmt.Sprintf("%v", response["hora"])

	subject := "Registro de ponto"
	message := "Registro de ponto efetuado com sucesso - data: " + data + " hora: " + hora

	log.Println("Topic arn: " + topicArn)

	defaultConfig, err := config.LoadDefaultConfig(context.TODO())

	if err != nil {
		panic("configuration error, " + err.Error())
	}

	snsClient := sns.NewFromConfig(defaultConfig)

	log.Println("Publishing...")
	result, err := snsClient.Publish(ctx, &sns.PublishInput{
		Message:  &message,
		Subject:  &subject,
		TopicArn: &topicArn,
	})

	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}

	log.Println(result.MessageId)
}
