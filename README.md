# Punch the clock - lambda

### Instruções para deploy

Comando para setar o sistema operacional para linux 
```shell
$env:GOOS = "linux"
```

Comando para setar a arquitetura do processador para amd64
```shell
$env:GOARCH = "amd64"
```

Comando para gerar executável do código
```shell
go build -o main
```

Crie um .zip do arquivo main que foi gerado e faça upload na aws
